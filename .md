*This is a rough English translation of [this article](http://www.nu.nl/internet/4342051/skype-krijgt-boete-in-belgie-weigeren-medewerking-in-politieonderzoek.html), originally published in Dutch by NU.nl on October 27, 2016.*

# Skype gets fined in Belgium for not cooperating in police investigation

__Skype (a Microsoft subsidiary) has been fined 30.000 euro in Belgium, for not being able to provide information about conversations in a police investigation.__

Authorities demanded that Skype provide information about conversations between certain users, but the company said that this was a technical impossibility.

Skype also felt that they were not bound by Belgian law, because their European headquarters are located in Luxembourg. The court disagreed, and [De Standaard](http://www.standaard.be/cnt/dmf20161027_02542314) reports that they were fined 30.000 euro on Thursday.

## Lawful interception

The investigation took place in 2012. Skype was able to provide information about the users in question like e-mail addresses, history and IP addresses. However, they were unable to provide anything regarding the contents of conversations.

According to the company, this was impossible because conversations are encrypted. Skype itself does not have access to their contents.

According to the court, this is irrelevant. Because Skype is active on the Belgian market, it has to comply with local legislation that requires them to support wiretapping. "It was clear that there was no intention to cooperate with the investigation," writes the court in its verdict.

## Consequences

The verdict can have significant consequences. According to the Mechelen court, all calling [VoIP] services in Belgium must be able to provide access to conversations. That would effectively ban *end-to-end encryption*, where the provider does not have access to conversations.

More and more services are using such encryption. Such services include WhatsApp - which plans to offer video calls in the future, as well.

Microsoft has released a statement, saying that they are considering further legal action. "We feel that legal procedures must also respect the privacy of citizens, and take into account international borders," according to the company/
